var http = require("http"), fs = require('fs');

//var admin = require("firebase-admin");
//var serviceAccount = require('pluggable-web-search.herokuapp.com/pluggable-web-search-firebase-adminsdk-7jyh0-a9a26674e5.json');
var firebase = require("firebase/app");
require("firebase/auth");
require("firebase/firestore");
require("firebase/database");
var firebaseConfig = {
    apiKey: "AIzaSyDLuzRZOqM-RKoFIlZnIaUADIcGQGMt76A",
    authDomain: "pluggable-web-search.firebaseapp.com",
    databaseURL: "https://pluggable-web-search.firebaseio.com",
    projectId: "pluggable-web-search",
    storageBucket: "pluggable-web-search.appspot.com",
    messagingSenderId: "818042513625",
    appId: "1:818042513625:web:116b6960c1264a97df36c8",
    measurementId: "G-HL5R410NNV"
};
firebase.initializeApp(firebaseConfig);
console.log("Firebase initialized");
//firebase.analytics();
var fstore = firebase.firestore();
console.log("Firestore connected");
// fstore.collection('queries').doc('MAMmKUd6MGkqXxszx1yQ').onSnapshot(function(doc) {
//     console.log("Current data: ", doc.data());
// });
// fstore.collection('queries').where("query", "==", "testquery").onSnapshot(function(querySnapshot) {
//     querySnapshot.forEach(function(doc) {
//         console.log("Current data: ", doc.data());
//     });
// });

var server = http.createServer(httphandler);
var port = process.env.PORT || 8080;
server.listen(port);

//admin.initializeApp({
//  credential: admin.credential.cert(serviceAccount),
//  databaseURL: "https://pluggable-web-search.firebaseio.com"
//});

function httphandler(request,response){
    if(request.url.indexOf('.json') != -1){
        fs.readFile('./pluggable-web-search-firebase-adminsdk-7jyh0-a9a26674e5.json', function (err, data) {
            if (err) console.log(err);
            response.writeHead(200, {'Content-Type': 'application/json'});
            response.write(data);
            response.end();
        });
    } else if(request.url.indexOf('.css') != -1){
        fs.readFile('./test.css', function (err, data) {
            if (err) console.log(err);
            response.writeHead(200, {'Content-Type': 'text/css'});
            response.write(data);
            response.end();
        });
    } else if(request.url.indexOf('.js') != -1){
        fs.readFile('./test.js', function (err, data) {
            if (err) console.log(err);
            response.writeHead(200, {'Content-Type': 'text/javascript'});
            response.write(data);
            response.end();
        });
    } else {
        fs.readFile('./index.html', function (err, data) {
            if (err) console.log(err);
            response.writeHead(200, {'Content-Type': 'text/html'});
            response.write(data);
            response.end();
        });
    }
}

console.log("ChatServer is running at port: " + port);
var io = require('socket.io');
var socketio = io.listen(server);
console.log("Socket.IO listening at port: " + port);
socketio.on("connection", function (socketclient) {
    console.log("client connected");
    socketclient.on("echo", function (search) {
        console.log("query requested");
        fstore.collection('queries').where("query", "==", search).onSnapshot(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {
                console.log("Current data: ", doc.data());
                socketclient.emit("echo", doc.data().query);
            });
        });
        // console.log("echoing back: " + search);
        // socketclient.emit("echo", search);
    });
});