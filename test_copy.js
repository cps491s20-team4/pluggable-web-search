var socketio;

(function(d) {
    var css = d.createElement('link');
    css.rel = 'stylesheet';
    css.href = 'https://pluggable-web-search.herokuapp.com/test.css';
    d.getElementsByTagName('head')[0].appendChild(css);

    var sc = d.createElement('script');
    sc.id = 'socketsource';
    sc.type = 'text/javascript';
    sc.async = true;
    sc.src = 'https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.0/socket.io.js';
    d.getElementsByTagName('head')[0].appendChild(sc);
    sc.onload = load();
    sc.onreadystatechange = load();
}(document));

function load(){
    while(document.getElementById('socketsource') == null){
        console.log("not there yet");
    }
    console.log("found it");

	socketio = io('https://pluggable-web-search.herokuapp.com/');
	
	var topdiv = document.createElement('div');
	topdiv.setAttribute('id', 'top');
	document.body.prepend(topdiv);
	
	var headerdiv = document.createElement('div');
	headerdiv.setAttribute('id', 'topheader');
	topdiv.appendChild(headerdiv);
	headerdiv.innerHTML = "Drag to move";
	
	var searchField = document.createElement('input');
	searchField.setAttribute('type', 'text');
	searchField.setAttribute('placeholder', 'Search page here...');
	searchField.setAttribute('id', 'search');
	searchField.setAttribute('onkeypress', 'entertoSearch(event)');
	topdiv.appendChild(searchField);

    var searchResults = document.createElement('div');
    searchResults.setAttribute('id', 'results');
    topdiv.appendChild(searchResults);
	
	dragElement(topdiv);
}

function entertoSearch(e){
	if(e.keyCode==13){
        echo();
		document.getElementById('search').value = '';
	}
}

function echo(){
    socketio.emit("echo", document.getElementById("search").value);
}

function dragElement(draggableElement) {
    var leftedge = 0, topedge = 0, rightedge = 0, bottomedge = 0;
    if (document.getElementById(draggableElement.id + "header")) {
        document.getElementById(draggableElement.id + "header").onmousedown = dragMouseDown;
    } else {
        draggableElement.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        rightedge = e.clientX;
        bottomedge = e.clientY;
        document.onmouseup = closeDragElement;
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        leftedge = rightedge - e.clientX;
        topedge = bottomedge - e.clientY;
        rightedge = e.clientX;
        bottomedge = e.clientY;
        draggableElement.style.top = (draggableElement.offsetTop - topedge) + "px";
        draggableElement.style.left = (draggableElement.offsetLeft - leftedge) + "px";
    }

    function closeDragElement() {
        document.onmouseup = null;
        document.onmousemove = null;
    }
    
    socketio.on("echo", (search) => {
        document.getElementById('results').innerHTML = search;
    });
}       